void eliminate_one_row(
	float* matrix,
	const unsigned long int size,
	const unsigned long int picking_row,
	const unsigned long int changing_row)
{
	if(fabs(matrix[changing_row*size + picking_row]) < 10e-3f) return;
	
	float factor = matrix[changing_row*size + picking_row]/matrix[picking_row*size+picking_row];

	for(unsigned long int column = 0; column < changing_row+1; ++column)
	{
		matrix[changing_row*size+column] = matrix[changing_row*size+column] - factor * matrix[picking_row*size+column];
	}
}


kernel void matrix_determinant(
	global float* matrix,
	global float* determinant,
	const unsigned long int size)
{
	const unsigned long int row = get_global_id(0);

	for(unsigned long int m_row = 0; m_row < size; ++m_row)
	{
		if(row > m_row)
		{
			eliminate_one_row(matrix, size, m_row, row);
		}
	}

	*determinant *= matrix[row*size+row];
}