kernel void componentwise_binary_kernel(
	global float* vec1,
	global float* vec2,
	global float* output_vec,
	const unsigned long int row_size,
	const unsigned long int column_size)
{
	const unsigned int 	row_global_size = get_global_size(0), 	column_global_size = get_global_size(1),
						row = get_global_id(0), 				column = get_global_id(1);

	if(row_global_size >= row_size && column_global_size >= column_size && row < row_size && column < column_size) {
		output_vec[row*row_size+column] = vec1[row*row_size+column] OPERATOR vec2[row*row_size+column];
	}
	
}