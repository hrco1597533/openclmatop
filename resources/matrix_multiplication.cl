kernel void matrix_multiplication(
		global float* matrix1,
		global float* matrix2,
		global float* output_matrix,
		const unsigned int column_size1, 
		const unsigned int row_size2,
		const unsigned int same_length)
{
	const size_t row = get_global_id(0U), column = get_global_id(1U);
	
	private float sum = 0.0f;

    for(private unsigned int i = 0; i<same_length; ++i) {
		//printf("row: %u\t column: %u\tlocal_row: %u\tlocal_column: %u\n", row, column, local_row, local_column);
    	sum += (matrix1[row*same_length+i] * matrix2[same_length*i+column]);
    }
    //printf("result[%llu][%llu] = %f\n", row, column, sum);
    output_matrix[row*row_size2+column] = sum;
}