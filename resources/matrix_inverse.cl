void eliminate_row(
	float* matrix,
	const unsigned long int size,
	const unsigned long int picking_row,
	const unsigned long int changing_row)
{
	if(fabs(matrix[changing_row*2*size + picking_row]) < 10e-6f) return;
	
	float factor = matrix[changing_row*2*size + picking_row]/matrix[picking_row*2*size+picking_row];
	//printf("changing_row: %lu\tm1: %f\tm2: %f\n", changing_row, matrix[changing_row*2*size + picking_row], matrix[picking_row*2*size+picking_row]);

	for(unsigned long int column = 0; column < 2*size; ++column)
	{
		matrix[changing_row*2*size+column] = matrix[changing_row*2*size+column] - factor * matrix[picking_row*2*size+column];
	}
}

// number of rows thread kernel
kernel void parallel_single_elimination(
	global float* matrix,
	const unsigned long int size,
	const unsigned long int curr_row,
	global char* flag)
{
	const unsigned long int row = get_global_id(0);

	//printf("size: %lu\tcurr_row: %lu\trow: %lu\n", size, curr_row, row);

	if(row != curr_row)
	{
		eliminate_row(matrix, size, curr_row, row);
	}
}







char switch_rows(
	float *mat,
	const unsigned long int size,
	const unsigned long int next_row)
{
	float temp;
	unsigned long int r_row = next_row+1;

	for(;r_row < size && fabs(mat[r_row*2*size+next_row]) <10e-3f; ++r_row);
	if(r_row >= size)
	{
		//printf("Here0\n");
		return 0;
	}
	for(unsigned long int col = 0; col < 2*size; ++col)
	{
		temp = mat[next_row*size+col];
		mat[next_row*size+col] = mat[r_row*size+col];
		mat[r_row*size+col] = temp;
	}
		
	//printf("Here1\n");
	return 1;
}

// One thread kernel
kernel void row_check(
	global float* mat,
	const unsigned long int size,
	const unsigned long int next_row,
	global char* flag)
{
/*
	for(size_t row = 0; row < size; ++row)
	{
		for(size_t col = 0; col<size*2; ++col)
		{
			printf("%f ", mat[row*2*size+col]);
		}
		printf("\n");
	}
	printf("\n");
*/	
	if(fabs(mat[next_row*2*size+next_row]) > 10e-3f)
	{
		*flag = 1;
		return;
	}

	*flag = switch_rows(mat, size, next_row);
}
