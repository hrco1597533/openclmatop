kernel void matrix_transpose(
	global float* matrix,
	global float* output_vec,
	const unsigned long int row_size,
	const unsigned long int column_size)
{
	const unsigned int 	row = get_global_id(0), column = get_global_id(1);
	
	output_vec[column*column_size+row] = matrix[row*row_size+column];
}