#include "timer"

Timer::Timer()
	: consumer([](unsigned long int ns){ std::cout << "Time: " << ns << "ns" << std::endl; }),
	  start_time(std::chrono::high_resolution_clock::now())
{

}

Timer::Timer(std::function<void(unsigned long int ns)>&& consumer)
	: consumer(consumer), start_time(std::chrono::high_resolution_clock::now()) 
{

}

Timer::Timer(const std::function<void(unsigned long int ns)>& consumer)
	: consumer(consumer), start_time(std::chrono::high_resolution_clock::now())
{

}

Timer::~Timer()
{
	consumer(std::chrono::duration_cast<std::chrono::nanoseconds>
				(std::chrono::high_resolution_clock::now() - start_time)
				.count()
			);
}
