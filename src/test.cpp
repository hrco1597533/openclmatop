#include <iostream>
#include <algorithm>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>

#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120

#include "CL/cl2.hpp"
#include "loguru.hpp"
#include "sequential_algorithms"
#include "parallel_algorithms"
#include "timer"
#include "OpenCLI"
#include "matrix"

#define REPEAT(expression, n_times) for(auto i = 0UL; i<n_times; ++i) expression
#define PRINT_VECTOR(vec) std::for_each(vec.begin(), vec.end(), [](const auto elem) { std::cout << elem << " "; } )

#define N 1000
#define K 1000

static void fill_data(Matrix<float> &matrix);
static void do_something_with_result(Matrix<float> &result);

int main(int argc, char *argv[])
{

	loguru::init(argc, argv);

	Matrix<float> mat1(K, K);
	Matrix<float> mat2(K, K);
	Matrix<float> res(K, K);

	fill_data(mat1); fill_data(mat2);

	{
		using namespace sequential;

		Timer t([](auto ns){ LOG_F(INFO, "sequential time: %f", ns/1000000.0); });

		for(std::size_t i = 0; i<N; ++i)
		{
			res = multiply(mat1, mat2);
			do_something_with_result(res);
		}
	}

	{
		using namespace parallel;

		Timer t([](auto ns){ LOG_F(INFO, "parallel time: %f", ns/1000000.0); });

		for(std::size_t i = 0; i<N; ++i)
		{
			res = multiply(mat1, mat2);
			do_something_with_result(res);
		}
	}

	return 0;
}


static void fill_data(Matrix<float> &matrix)
{
	std::generate(matrix.begin(), matrix.end(), [](){ static float f = 0.5f; return f+=0.5f;});
}

static void do_something_with_result(Matrix<float> &result)
{

}