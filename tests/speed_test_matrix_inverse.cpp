#include "speed_test_matrix_inverse.hpp"

#include <algorithm>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <ctime>

#include <iostream>

#include "sequential_algorithms"
#include "parallel_algorithms"
#include "matrix"
#include "timer"

#define PRINT_VECTOR(vec) std::for_each(vec.begin(), vec.end(), [](auto elem) { std::cout << elem << ' '; }); std::cout << '\n'

typedef Matrix<float>(*algorithm_t)(const Matrix<float>&);

static void fill_data(Matrix<float> &matrix);
static TestResult one_test_inverse(std::size_t size, algorithm_t function_seq, algorithm_t function_para);

std::pair<std::string, std::vector<TestResult>>
SpeedTestMatrixInverse::operator()()
{
	std::vector<TestResult> results;
	std::size_t max_matrix_size = 100,
				min_matrix_size = 10,
				matrix_delta_size = 10;

	algorithm_t seq_algorithm = sequential::inverse;
	algorithm_t para_algorithm = parallel::inverse;

	for(std::size_t size = min_matrix_size; size <= max_matrix_size; size+=matrix_delta_size)
	{
			results.push_back(one_test_inverse(size, seq_algorithm, para_algorithm));
	}

	return std::make_pair("Inverse", std::move(results));
}

static TestResult
one_test_inverse(
	std::size_t size, 
	algorithm_t function_seq,
	algorithm_t function_para)
{
	TestResult result;
	result.mat1_width = result.mat2_width = size;
	result.mat1_height = result.mat2_height = size;

	Matrix<float> mat(size, size);
	Matrix<float> res_seq(size, size), res_para(size, size);

	fill_data(mat);

	{
		Timer t([&](auto ns){ result.seq_ns = ns; });
		res_seq = function_seq(mat);
	}

	{
		Timer t([&](auto ns){ result.para_ns = ns; });
		res_para = function_para(mat);
	}

	result.is_same = std::equal(
		res_seq.begin(),
		res_seq.end(),
		res_para.begin(),
		[](auto e1, auto e2) { return std::abs(e1-e2) < 10e-3f; }
	);

	std::cout << "dimension: " << result.mat1_width << "\tIs the same: " << (result.is_same ? "TRUE" : "FALSE") << '\n'; 
	if(!result.is_same)
	{
		std::cout << res_seq << '\n';

		std::cout << res_para << '\n';
	}

	return result;
}

static void fill_data(Matrix<float> &matrix)
{
	std::srand(std::time(nullptr));
	for(std::size_t row = 0, column; row < matrix.row_number; ++row)
	{
		for(column = 0; column < matrix.column_number; ++column)
		{
			if(row == column) matrix.at(row,column) = 1.0f;
			else if(row > column) matrix.at(row, column) = (std::rand()%5)/7.0f;
		}
	}
}

