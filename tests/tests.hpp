#ifndef TESTS_HPP
#define TESTS_HPP

#include <vector>
#include <utility>
#include <string>

struct TestResult
{
	std::size_t mat1_width, mat1_height, mat2_width, mat2_height;
	std::size_t seq_ns, para_ns;
	bool is_same;
};

struct Test
{
	virtual std::pair<std::string, std::vector<TestResult>> operator()()=0;
};

#endif // TESTS_HPP