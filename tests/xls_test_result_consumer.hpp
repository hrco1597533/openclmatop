#ifndef XLS_TEST_RESULT_CONSUMER_HPP
#define XLS_TEST_RESULT_CONSUMER_HPP

#include <string>

#include "test_result_consumer.hpp"
#include "libxl.h"

struct XlsTestResultConsumer : public TestResultConsumer 
{
	XlsTestResultConsumer(const char*);
 	virtual ~XlsTestResultConsumer();
	void operator()(const std::pair<std::string, std::vector<TestResult>>& result) override;

private:
	libxl::Book *m_book;
	std::string m_book_name;
};

#endif // XLS_TEST_RESULT_CONSUMER_HPP