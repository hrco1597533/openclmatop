#include "xls_generator.hpp"

#include <algorithm>
#include <iostream>

#include "libxl.h"

void 
generate_xls_documents(
	TestExecutor& executor,
	TestResultConsumer& consumer)
{
	std::for_each(
		executor.begin(),
		executor.end(),
		[&consumer] (const auto& pair) mutable
		{
			auto result = (*pair.second)();
			consumer(result);
		}
	);
}
