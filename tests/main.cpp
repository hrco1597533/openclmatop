#include <unordered_set>
#include "xls_generator.hpp"
#include "test_executor.hpp"
#include "xls_test_result_consumer.hpp"

int main(int argc, const char **argv)
{
	TestExecutor& executor = TestExecutor::instance();
	XlsTestResultConsumer consumer = XlsTestResultConsumer("Test1.xls");

	generate_xls_documents(executor, consumer);

	return 0;
}