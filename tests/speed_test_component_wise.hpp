#ifndef SPEED_TEST_COMPONENT_WISE_HPP
#define SPEED_TEST_COMPONENT_WISE_HPP

#include "test_executor.hpp"
#include "tests.hpp"

class SpeedTestComponentWise : public Test
{
	static std::size_t index; 
	static void* _creator_func()
	{
		return new SpeedTestComponentWise();
	}
public:
	std::pair<std::string, std::vector<TestResult>> operator()() override;
};

//std::size_t SpeedTestComponentWise::index = TestExecutor::instance().registerCreator("SpeedTestComponentWise", SpeedTestComponentWise::_creator_func); 

#endif // SPEED_TEST_COMPONENT_WISE_HPP