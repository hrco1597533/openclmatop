#ifndef XLS_GENERATOR_HPP
#define XLS_GENERATOR_HPP

#include "test_executor.hpp"
#include "test_result_consumer.hpp"

void generate_xls_documents(TestExecutor& executor, TestResultConsumer& consumer);

#endif // XLS_GENERATOR_HPP