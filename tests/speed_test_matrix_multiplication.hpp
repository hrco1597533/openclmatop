#ifndef SPEED_TEST_MATRIX_MULTIPLICATION_HPP
#define SPEED_TEST_MATRIX_MULTIPLICATION_HPP

#include "test_executor.hpp"
#include "tests.hpp"

class SpeedTestMatrixMultiplication : public Test
{
	static std::size_t index; 
	static void* _creator_func()
	{
		return new SpeedTestMatrixMultiplication();
	}
public:
	std::pair<std::string, std::vector<TestResult>> operator()() override;
};

//std::size_t SpeedTestMatrixMultiplication::index = TestExecutor::instance().registerCreator("SpeedTestMatrixMultiplication", SpeedTestMatrixMultiplication::_creator_func); 


#endif // SPEED_TEST_MATRIX_MULTIPLICATION_HPP