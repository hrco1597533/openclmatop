#include "xls_test_result_consumer.hpp"

#include <iostream>

XlsTestResultConsumer::XlsTestResultConsumer(const char* book_name)
	:	m_book(xlCreateBook()),
		m_book_name(book_name)
{
	if(!m_book)
	{
		std::cerr << "Can not open a book\n";
	}
}
XlsTestResultConsumer::~XlsTestResultConsumer()
{
	if(!m_book->save(m_book_name.c_str()))
	{
		std::cerr << "Can not save a book\n";
	}
	else
	{
		std::cout << '"' << m_book_name << "\" is saved\n";
	}

	m_book->release();
}

void 
XlsTestResultConsumer::operator()(
	const std::pair<std::string, std::vector<TestResult>>& result)
{
	using namespace libxl;

	auto [test_name, results] = result;
	std::size_t row = 1, column = 0;

	Sheet *sheet = m_book->addSheet(test_name.c_str());
	if(!sheet)
	{
		std::cerr << "Cannot open new sheet\n";
		return;
	}

	sheet->writeStr(row,column++, "Matrix1_width");
	sheet->writeStr(row,column++, "Matrix1_height");
	sheet->writeStr(row,column++, "Matrix2_width");
	sheet->writeStr(row,column++, "Matrix2_height");
	sheet->writeStr(row,column++, "Sequntial operation[ns]");
	sheet->writeStr(row,column++, "Paralel operation[ns]");
	sheet->writeStr(row,column, "Are the same?");

	for(const auto& test_result : results)
	{
		row++; column=0;
		sheet->writeNum(row, column++, test_result.mat1_width);
		sheet->writeNum(row, column++, test_result.mat1_height);
		sheet->writeNum(row, column++, test_result.mat2_width);
		sheet->writeNum(row, column++, test_result.mat2_height);
		sheet->writeNum(row, column++, test_result.seq_ns);
		sheet->writeNum(row, column++, test_result.para_ns);
		sheet->writeStr(row, column, (test_result.is_same ? "True" : "False"));
	}
}
