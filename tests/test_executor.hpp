#ifndef TEST_EXECUTOR_HPP
#define TEST_EXECUTOR_HPP

#include <unordered_map>
#include <memory>

#include "tests.hpp"

class TestExecutor
{
public:
	typedef void* (*creatorFunc)();

	static TestExecutor& instance();

	std::size_t registerCreator(const std::string& name, creatorFunc func);
	auto begin(){ return m_tests.begin(); }
	auto end(){ return m_tests.end(); }
private:
	static inline std::size_t counter = 0;
	TestExecutor()=default;
	~TestExecutor()=default;
	TestExecutor(const TestExecutor&)=default;
	std::vector<std::pair<std::string, std::unique_ptr<Test>>> m_tests;
};

#endif // TEST_EXECUTOR_HPP