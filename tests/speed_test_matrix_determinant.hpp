#ifndef SPEED_TEST_MATRIX_DETERMINANT_HPP
#define SPEED_TEST_MATRIX_DETERMINANT_HPP

#include "test_executor.hpp"
#include "tests.hpp"

class SpeedTestMatrixDeterminant : public Test
{
	static std::size_t index; 
	static void* _creator_func()
	{
		return new SpeedTestMatrixDeterminant();
	}
public:
	std::pair<std::string, std::vector<TestResult>> operator()() override;
};

//std::size_t SpeedTestMatrixDeterminant::index = TestExecutor::instance().registerCreator("SpeedTestMatrixDeterminant", SpeedTestMatrixDeterminant::_creator_func); 

#endif // SPEED_TEST_MATRIX_DETERMINANT_HPP