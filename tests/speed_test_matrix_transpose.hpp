#ifndef SPEED_TEST_MATRIX_TRANSPOSE_HPP
#define SPEED_TEST_MATRIX_TRANSPOSE_HPP

#include "test_executor.hpp"
#include "tests.hpp"

class SpeedTestMatrixTranspose : public Test
{
	static std::size_t index;
	static void* _creator_func()
	{
		return new SpeedTestMatrixTranspose();
	}
public:
	std::pair<std::string, std::vector<TestResult>> operator()() override;
};

//std::size_t SpeedTestMatrixTranspose::index = TestExecutor::instance().registerCreator("SpeedTestMatrixTranspose", SpeedTestMatrixTranspose::_creator_func); 

#endif // SPEED_TEST_MATRIX_TRANSPOSE_HPP