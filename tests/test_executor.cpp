#include "test_executor.hpp"

TestExecutor& TestExecutor::instance()
{
	static TestExecutor test_executor;
	return test_executor;
}

std::size_t TestExecutor::registerCreator(const std::string& name, creatorFunc func)
{
	Test* test_ptr = (Test*)func();
	m_tests.push_back(std::make_pair(name, std::unique_ptr<Test>(test_ptr)));
	return TestExecutor::counter++;
}

