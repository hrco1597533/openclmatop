#ifndef SPEED_TEST_MATRIX_INVERSE_HPP
#define SPEED_TEST_MATRIX_INVERSE_HPP

#include "test_executor.hpp"
#include "tests.hpp"

class SpeedTestMatrixInverse : public Test
{
	static std::size_t index; 
	static void* _creator_func()
	{
		return new SpeedTestMatrixInverse();
	}
public:
	std::pair<std::string, std::vector<TestResult>> operator()() override;
};

std::size_t SpeedTestMatrixInverse::index = TestExecutor::instance().registerCreator("SpeedTestMatrixInverse", SpeedTestMatrixInverse::_creator_func); 

#endif // SPEED_TEST_MATRIX_INVERSE_HPP