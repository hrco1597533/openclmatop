#include "speed_test_matrix_multiplication.hpp"

#include <algorithm>
#include <vector>
#include <cmath>

#include "sequential_algorithms"
#include "parallel_algorithms"
#include "matrix"
#include "timer"

typedef Matrix<float>(*algorithm_t)(const Matrix<float>&, const Matrix<float>&);

static void fill_data(Matrix<float> &matrix);
static TestResult one_test_matrix_multiplication(std::size_t mat_width, std::size_t mat_height, algorithm_t function_seq, algorithm_t function_para);

std::pair<std::string, std::vector<TestResult>>
SpeedTestMatrixMultiplication::operator()()
{
	std::vector<TestResult> results;
	std::size_t max_matrix_width = 1000, max_matrix_height = 1000,
				min_matrix_width = 10, min_matrix_height = 10,
				matrix_delta_width = 10, matrix_delta_height = 10;
	algorithm_t seq_algorithm = sequential::operator*;
	algorithm_t para_algorithm = parallel::operator*;

	for(std::size_t width = min_matrix_width, height = min_matrix_height; width <= max_matrix_width && height <= max_matrix_height; width+=matrix_delta_width, height += matrix_delta_height)
	{
			results.push_back(one_test_matrix_multiplication(width, height, seq_algorithm, para_algorithm));
	}

	return std::make_pair("Matrix multiplication", std::move(results));
}

static TestResult
one_test_matrix_multiplication(
	std::size_t mat_width, 
	std::size_t mat_height, 
	algorithm_t function_seq,
	algorithm_t function_para)
{
	TestResult result;
	result.mat1_width = result.mat2_width = mat_width;
	result.mat1_height = result.mat2_height = mat_height;

	Matrix<float> 	mat1(mat_width, mat_height),
					mat2(mat_width, mat_height),
					res_seq(mat_width, mat_height),
					res_para(mat_width, mat_height);

	fill_data(mat1); fill_data(mat2);

	{
		Timer t([&](auto ns){ result.seq_ns = ns; });
		res_seq = function_seq(mat1, mat2);
	}

	{
		Timer t([&](auto ns){ result.para_ns = ns; });
		res_para = function_para(mat1, mat2);
	}

	result.is_same =
		std::equal(
			res_para.begin(),
			res_para.end(),
			res_seq.begin(),
			[](auto elem1, auto elem2){ return std::abs(elem1-elem2) < 10e-3f; }
		);

	return result;
}

static auto get_generator(float begin, float step)
{
	return [=, next = 0.0f]() mutable
	{
		next = begin;
		begin+=step;
		begin=-begin;
		return next;
	};
}

static void fill_data(Matrix<float> &matrix)
{
	std::generate(matrix.begin(), matrix.end(), get_generator(0.0f, 0.001f));
}

