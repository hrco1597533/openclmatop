#ifndef TEST_RESULT_CONSUMER_HPP
#define TEST_RESULT_CONSUMER_HPP

#include <vector>
#include <utility>
#include <string>

#include "tests.hpp"

struct TestResultConsumer
{
	virtual void operator()(const std::pair<std::string, std::vector<TestResult>>& result)=0;
	virtual ~TestResultConsumer()=default;
};

#endif // TEST_RESULT_CONSUMER_HPP