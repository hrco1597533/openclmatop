#include <algorithm>
#include <stdexcept>
#include <cstring>
#include <initializer_list>
#include <ostream>

template<typename T>
Matrix<T>::Matrix(std::size_t row_number, std::size_t column_number)
	:	row_number(row_number),
		column_number(column_number),
		matrix(new T[row_number*column_number])
{
	std::memset(matrix, 0, row_number*column_number*sizeof(T));
}

template<typename T>
Matrix<T>::Matrix(std::size_t row_number, std::size_t column_number, std::initializer_list<T>&& elems)
	:	row_number(row_number),
		column_number(column_number),
		matrix(new T[row_number*column_number])
{
	if(elems.size() != row_number*column_number)
		throw std::runtime_error("Dimensions are not valid");
	std::copy(std::begin(elems), std::end(elems), std::begin(*this));
}

template<typename T>
Matrix<T>::Matrix(const Matrix& matrix)
	:	row_number(matrix.row_number),
		column_number(matrix.column_number),
		matrix(new T[row_number*column_number])
{
	std::copy(std::begin(matrix), std::end(matrix), std::begin(*this));
}

template<typename T>
Matrix<T>::Matrix(Matrix&& matrix)
	:	row_number(matrix.row_number),
		column_number(matrix.column_number),
		matrix(std::begin(matrix))
{
	matrix.matrix = nullptr;
}

template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& matrix)
{
	if(matrix.row_number != row_number || matrix.column_number != column_number) 
		throw std::runtime_error("Dimensions are not valid");

	std::copy(std::begin(matrix), std::end(matrix), std::begin(*this));

	return *this;
}

template<typename T>
Matrix<T>& Matrix<T>::operator=(Matrix&& matrix)
{
	if(matrix.row_number != row_number || matrix.column_number != column_number) 
		throw std::runtime_error("Dimensions are not valid");

	delete[] this->matrix;

	this->matrix = matrix.matrix;

	matrix.matrix = nullptr;

	return *this;
}

template<typename T>
T& Matrix<T>::at(std::size_t x, std::size_t y)
{	
	return (*this)[x*column_number+y];
}

template<typename T>
const T& Matrix<T>::at(std::size_t x, std::size_t y) const
{
	return (*this)[x*column_number+y];
}

template<typename T>
Matrix<T>::~Matrix()
{
	delete[] matrix;
}

template<typename T>
T& Matrix<T>::operator[](std::size_t index) 
{
	if(index >= row_number*column_number)
		throw std::runtime_error(std::string("Illegal indexes ")+std::to_string(index));
	else 
		return matrix[index];	
}

template<typename T>
T& Matrix<T>::operator[](std::size_t index) const
{
	if(index >= row_number*column_number)
		throw std::runtime_error(std::string("Illegal indexes ")+std::to_string(index));
	else 
		return matrix[index];	
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& matrix) {
	for(std::size_t i = 0; i<matrix.row_number; ++i) {
		for(std::size_t j = 0; j<matrix.column_number; ++j) {
			os << matrix.at(i,j) << ' ';
		}
		os << std::endl;
	}
	return os;
}
