SRC_DIR=src
DEPS_DIR=deps
BUILD_DIR=build
BIN_DIR=bin
TEST_DIR=tests

OTHER_INCLUDE_DIRS=-Ideps/loguru/include -Ideps/OpenCL/include -Ideps/matrix -Ideps/timer -Ideps/OpenCL_interface -Isrc -Ideps/libxl/include
LIBS=-lOpenCL -lpthread -ldl -lxl -lloguru
FLAGS=-g -std=c++17 -Wall -D CL_HPP_TARGET_OPENCL_VERSION=120 -D CL_HPP_MINIMUM_OPENCL_VERSION=120
LIBS_DIRS=-Ldeps/libxl/lib -Ldeps/loguru/lib

SOURCES=$(shell find $(SRC_DIR) -name '*.cpp')
OBJECTS=$(addprefix $(BUILD_DIR)/code/,$(SOURCES:$(SRC_DIR)/%.cpp=%.o))

TEST_SOURCES=$(shell find $(TEST_DIR) -name '*.cpp')
TEST_OBJECTS=$(addprefix $(BUILD_DIR)/tests/,$(TEST_SOURCES:$(TEST_DIR)/%.cpp=%.o))

EXE_NAME=main
TESTS_EXE_NAME=run_tests

CC=g++

all: build_project build_tests
	$(CC) -o $(BIN_DIR)/$(EXE_NAME) $(FLAGS) $(OBJECTS) $(OTHER_INCLUDE_DIRS) $(LIBS_DIRS) $(LIBS)

build_project: bin_dir build_dir $(OBJECTS)
	$(CC) -o $(BIN_DIR)/$(EXE_NAME) $(FLAGS) $(OBJECTS) $(OTHER_INCLUDE_DIRS) $(LIBS_DIRS) $(LIBS)	

build_tests: bin_dir build_dir $(TEST_OBJECTS)
	$(CC) -o $(BIN_DIR)/$(TESTS_EXE_NAME) $(FLAGS) $(TEST_OBJECTS) $(OTHER_INCLUDE_DIRS) $(LIBS_DIRS) $(LIBS)

$(BUILD_DIR)/code/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(FLAGS) $(OTHER_INCLUDE_DIRS) $(LIBS_DIRS) $(LIBS) -c $< -o $@ 

$(BUILD_DIR)/tests/%.o: $(TEST_DIR)/%.cpp
	$(CC) $(FLAGS) $(OTHER_INCLUDE_DIRS) $(LIBS_DIRS) $(LIBS) -c $< -o $@






clean:
	rm -rf *.o preprocessed *.core *.xls $(BIN_DIR) $(BUILD_DIR)

















build_dir:
	if [ ! -d "$(BUILD_DIR)" ]; then mkdir -p $(BUILD_DIR) $(BUILD_DIR)/tests $(BUILD_DIR)/code; fi

bin_dir:
	if [ ! -d "$(BIN_DIR)" ]; then mkdir -p $(BIN_DIR); fi
	